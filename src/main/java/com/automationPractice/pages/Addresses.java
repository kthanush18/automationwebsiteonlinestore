package com.automationPractice.pages;

import org.jsoup.parser.Parser;
import org.openqa.selenium.By;

import com.web.BrowserUtility;

public class Addresses extends BrowserUtility {
	
	private static final By ADDADDRESS_BUTTON_LOCATOR = By.xpath("//a [@title = 'Add an address']");
	private static final By COMPANY_TEXTBOX_LOCATOR = By.id("company");
	private static final By ADDRESS_TEXTBOX_LOCATOR = By.id("address1");
	private static final By ADDRESS2_TEXTBOX_LOCATOR = By.id("address2");
	private static final By CITY_TEXTBOX_LOCATOR = By.id("city");
	private static final By STATE_DROPDOWN_LOCATOR = By.id("id_state");
	private static final By ZIPCODE_TEXTBOX_LOCATOR = By.id("postcode");
	private static final By HOMEPHONE_TEXTBOX_LOCATOR = By.id("phone");
	private static final By MOBILEPHONE_TEXTBOX_LOCATOR = By.id("phone_mobile");
	private static final By ADDITIONALINFO_TEXTBOX_LOCATOR = By.id("other");
	private static final By ADDRESSTITLE_TEXTBOX_LOCATOR = By.id("alias");
	private static final By SAVE_BUTTON_LOCATOR = By.id("submitAddress");
	private static final By HOME_BUTTON_LOCATOR = By.xpath("//span [text() = ' Home']/..");
	
	public void addNewAddress(String company,String address,String address2,String city,String state,String zipcode,String homephone,String mobilephone,String additionalinfo,String addresstitle) {
		waitAndClick(ADDADDRESS_BUTTON_LOCATOR);
		waitAndEnterText(COMPANY_TEXTBOX_LOCATOR, company);
		waitAndEnterText(ADDRESS_TEXTBOX_LOCATOR, address);
		waitAndEnterText(ADDRESS2_TEXTBOX_LOCATOR, address2);
		waitAndEnterText(CITY_TEXTBOX_LOCATOR, city);
		selectByValueInDropDown(STATE_DROPDOWN_LOCATOR, Integer.parseInt(state));
		waitAndEnterText(ZIPCODE_TEXTBOX_LOCATOR, zipcode);
		waitAndEnterText(HOMEPHONE_TEXTBOX_LOCATOR, homephone);
		waitAndEnterText(MOBILEPHONE_TEXTBOX_LOCATOR, mobilephone);
		waitAndEnterText(ADDITIONALINFO_TEXTBOX_LOCATOR, additionalinfo);
		waitAndEnterText(ADDRESSTITLE_TEXTBOX_LOCATOR, addresstitle);
		waitAndClick(SAVE_BUTTON_LOCATOR);
		waitAndClick(HOME_BUTTON_LOCATOR);
	}

}
