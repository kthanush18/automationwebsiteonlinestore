package com.automationPractice.pages;

import org.openqa.selenium.By;

import com.web.BrowserUtility;

public class DashBoard extends BrowserUtility{
	
	private static final By MYADDRESSES_BUTTON_LOCATOR = By.xpath("//a [@title = 'Addresses']/..");
	private static final By WOMEN_LINK_LOCATOR = By.xpath("//a [@title = 'Women']/..");
	private static final By DRESSES_LINK_LOCATOR = By.xpath("//div [@class = 'subcategory-image']/a [@title = 'Dresses']");
	private static final By SUMMERDRESSES_LINK_LOCATOR = By.xpath("//div [@class = 'subcategory-image']/a [@title = 'Summer Dresses']");
	private static final By LIST_LINK_LOCATOR = By.xpath("//i [@class = 'icon-th-list']");
	private static final By ADDTOCART_LOCATOR = By.xpath("//a [@data-id-product = '5' and @title = 'Add to cart']");
	private static final By PROCEEDTOCHECKOUT_LOCATOR = By.xpath("//a [@title = 'Proceed to checkout']");
	private static final By ADDQUANTITY_LOCATOR = By.id("cart_quantity_up_5_19_0_443685");
	private static final By PROCEEDTOCHECKOUT2_LOCATOR = By.xpath("//p [@class = 'cart_navigation clearfix']/a[1]");
	private static final By PROCEEDTOCHECKOUT3_LOCATOR = By.name("processAddress");
	private static final By CHECKBOX_LOCATOR = By.xpath("//label [@for=\"cgv\"]");
	private static final By PROCEEDTOCHECKOUT4_LOCATOR = By.name("processCarrier");
	private static final By PAYUSINGCHECK_LOCATOR = By.xpath("//a [@title = 'Pay by check.']");
	private static final By CONFIRMORDER_LOCATOR = By.xpath("//span [text() = 'I confirm my order']/..");
	private static final By MYACCOUNT_LOCATOR = By.xpath("//a [@title = 'View my customer account']");
	private static final By MYORDERS_LOCATOR = By.xpath("//a [@title = 'Orders']");
	private static final By ORDERHISTORY_LOCATOR = By.xpath("//h1 [text() = 'Order history']");
	private static final By LOGOUT_LOCATOR = By.xpath("//a [@title = 'Log me out']");
	
	public void goToMyAddresses() {
		waitAndClick(MYADDRESSES_BUTTON_LOCATOR);
	}
	
	public void addProductsAndCheckOut() {
		waitAndClick(WOMEN_LINK_LOCATOR);
		waitAndClick(DRESSES_LINK_LOCATOR);
		waitAndClick(SUMMERDRESSES_LINK_LOCATOR);
		waitAndClick(LIST_LINK_LOCATOR);
		waitAndClick(ADDTOCART_LOCATOR);
		waitAndClick(PROCEEDTOCHECKOUT_LOCATOR);
		for (int count = 1; count <=4; count++) {
			waitAndClick(ADDQUANTITY_LOCATOR);
		}
		waitAndClick(PROCEEDTOCHECKOUT2_LOCATOR);
		waitAndClick(PROCEEDTOCHECKOUT3_LOCATOR);
		waitAndClick(CHECKBOX_LOCATOR);
		waitAndClick(PROCEEDTOCHECKOUT4_LOCATOR);
		waitAndClick(PAYUSINGCHECK_LOCATOR);
		waitAndClick(CONFIRMORDER_LOCATOR);
	}

	public void goToMyOrders() {
		waitAndClick(MYACCOUNT_LOCATOR);
		waitAndClick(MYORDERS_LOCATOR);
	}
	
	public boolean isOrderHistoryVisible() {
		return isElementVisible(ORDERHISTORY_LOCATOR);
	}
	
	public void logOut() {
		waitAndClick(LOGOUT_LOCATOR);
	}
}
