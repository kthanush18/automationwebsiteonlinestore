package com.automationPractice.pages;

import org.openqa.selenium.By;

import com.web.BrowserUtility;

public class Home extends BrowserUtility{
	
	protected static Home _home;
	protected static Login _login;
	protected static DashBoard _dashboard;
	protected static Addresses _addresses;
	
	private static final By SIGNIN_LINK_LOCATOR = By.xpath("//a [@class='login']");
	
	public void goToLoginPage() {
		clickOn(SIGNIN_LINK_LOCATOR);
		_login = new Login();
		_dashboard = new DashBoard();
		_addresses = new Addresses();
	}

}
