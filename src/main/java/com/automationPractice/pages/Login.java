package com.automationPractice.pages;

import org.openqa.selenium.By;

import com.web.BrowserUtility;

public class Login extends BrowserUtility {
	
	private static final By LOGINEMAIL_TEXTBOX_LOCATOR = By.id("email");
	private static final By LOGINPASSWORD_TEXTBOX_LOCATOR = By.id("passwd");
	private static final By LOGIN_BUTTON_LOCATOR = By.id("SubmitLogin");
	
	public void loginToApplication(String email,String password) {
		enterText(LOGINEMAIL_TEXTBOX_LOCATOR, email);
		enterText(LOGINPASSWORD_TEXTBOX_LOCATOR, password);
		clickOn(LOGIN_BUTTON_LOCATOR);
	}

	
}
