package com.web;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.NewSessionPayload;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.html.HTMLSelectElement;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BrowserUtility {
	public static WebDriver _driver;
	
	public void launchLocalBrowser(BrowserType browserType) {
		try {
			switch(browserType) {
			case CHROME:
				WebDriverManager.chromedriver().setup();
				_driver = new ChromeDriver();
				break;
			case FIREFOX:
				WebDriverManager.firefoxdriver().setup();
				_driver = new FirefoxDriver();
				break;
			case IE:
				WebDriverManager.iedriver().setup();
				_driver = new InternetExplorerDriver();
				break;
			case EDGE:
				WebDriverManager.edgedriver().setup();
				_driver = new EdgeDriver();
				break;
			}
		}
		catch(Exception E) {
			System.out.println("Failed to launch browser!!");
		}
	}
	
	public void navigateToURL(String URL) {
		_driver.get(URL);
		_driver.manage().window().maximize();
	}
	
	public void clickOn(By selector) {
		_driver.findElement(selector).click();
	}
	
	public void enterText(By selector,String text) {
		_driver.findElement(selector).sendKeys(text);
	}

	public void selectTitleDropdownAndText(By selector,String text) {
		clickOn(selector);
		clickOn(By.xpath("//input [@value = '"+text+"']/../.."));
	}
	
	public void selectDropdownAndText(By selector,String text) {
		clickOn(selector);
		clickOn(By.xpath("//span [text() = '"+text+"']/../.."));
	}
	
	public String getAttributeValue(By selector,String attributeName) {
		((JavascriptExecutor) _driver).executeScript("arguments[0].scrollIntoView();",getElement(selector));
		return waitForWebelementToLoad(selector).getAttribute(attributeName);
	}
	
	public WebElement getElement(By selector) {
		return _driver.findElement(selector);
	}
	
	public void moveToSpecificElement(By selector) {
		((JavascriptExecutor) _driver).executeScript("arguments[0].scrollIntoView();",getElement(selector));
	}
	
	public WebElement waitForWebelementToLoad(By selector) {
		WebDriverWait wait = new WebDriverWait(_driver, 20);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
	}
	
	public void waitAndClick(By selector) {
		((JavascriptExecutor) _driver).executeScript("arguments[0].scrollIntoView();",getElement(selector));
		WebElement element = waitForWebelementToLoad(selector);
		element.click();
	}
	
	public void waitAndEnterText(By selector,String text) {
		((JavascriptExecutor) _driver).executeScript("arguments[0].scrollIntoView();",getElement(selector));
		WebElement element = waitForWebelementToLoad(selector);
		element.sendKeys(text);
	}
	
	public String waitAndGetAttributeValue(By selector,String attributeName) {
		return waitForWebelementToLoad(selector).getAttribute(attributeName);
	}
	
	public boolean isElementVisible(By selector) {
		((JavascriptExecutor) _driver).executeScript("arguments[0].scrollIntoView();",getElement(selector));
		return _driver.findElement(selector).isDisplayed();
	}
	
	public void selectByValueInDropDown(By selector,Integer index) {
		Select select = new Select(getElement(selector));
		select.selectByIndex(index);
	}
	
	public void quitBrowser() {
		_driver.close();
		_driver.quit();
	}
	
	public void takeScreenShot() {
		TakesScreenshot screenshot = ((TakesScreenshot)_driver);
		File imageFile = screenshot.getScreenshotAs(OutputType.FILE);
		String timestamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new java.util.Date());
		File localFilePath = new File(System.getProperty("user.dir")+ "//screenshots//image"+timestamp+".png");
		try {
			FileUtils.copyFile(imageFile, localFilePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
