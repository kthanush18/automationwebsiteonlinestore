package com.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import com.opencsv.CSVReader;

public class ReadFromFiles {
	
	public static Iterator<String []> readDataFromCSVFile(String CSVFileName){
		
		File csvFile = new File(System.getProperty("user.dir")+ "//testData//"+CSVFileName+"");
		FileReader csvFileReader = null;
		try {
			csvFileReader = new FileReader(csvFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CSVReader reader = new CSVReader(csvFileReader);
		Iterator<String []> readDataIterator = reader.iterator();
		readDataIterator.next();
		
		return readDataIterator;
	}

}
