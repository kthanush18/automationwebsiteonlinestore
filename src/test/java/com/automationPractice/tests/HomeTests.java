package com.automationPractice.tests;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.automationPractice.pages.Home;
import com.web.BrowserType;
import com.web.ReadMyProperties;

public class HomeTests extends Home {
	
	@BeforeMethod
	public void testIntialize() {
		_home = new Home();
		_home.launchLocalBrowser(BrowserType.CHROME);
		_home.navigateToURL(ReadMyProperties.getMyProperty("URL"));
	}
	
	@Test (testName = "buy a product online", description = "login and buy products from website",dataProviderClass = com.dataProviders.LoginDataProvider.class,dataProvider = "loginDataProviderCSV")
	public void TC_BuyProductsOnline_VerifyProductPurchase(String email,String password,String company,String address,String address2,String city,String state,String zipcode,String homephone,String mobilephone,String additionalinfo,String addresstitle) {
		_home.goToLoginPage();
		_login.loginToApplication(email, password);
		_dashboard.goToMyAddresses();
		_addresses.addNewAddress(company, address, address2, city, state, zipcode, homephone, mobilephone, additionalinfo, addresstitle);
		_dashboard.addProductsAndCheckOut();
		_dashboard.goToMyOrders();
		takeScreenShot();
		
		assertTrue(_dashboard.isOrderHistoryVisible());
	}
	
	@AfterMethod
	public void testCleanUp() {
		_dashboard.logOut();
		_home.quitBrowser();
	} 
}
