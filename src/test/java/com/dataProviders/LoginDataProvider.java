package com.dataProviders;

import java.util.Iterator;

import org.testng.annotations.DataProvider;

import com.web.ReadFromFiles;

public class LoginDataProvider {
	
	@DataProvider (name = "loginDataProviderCSV")
	public Iterator<String []> readLoginDataFromCSV(){
		Iterator<String []> loginDataIterator = ReadFromFiles.readDataFromCSVFile("loginData.csv");
		return loginDataIterator;
	}

}
